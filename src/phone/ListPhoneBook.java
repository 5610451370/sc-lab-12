package phone;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;

public class ListPhoneBook {
	
	String file;
	FileWriter fileWriter;
	
	ListPhoneBook(String file){
		this.file = file;
	}
	
	public void addNamePhone(){
		try {
			System.err.println("---ADD NAME PHONE---");
			InputStreamReader inReader = new InputStreamReader(System.in);
			BufferedReader buffer = new BufferedReader(inReader);
		
			fileWriter = new FileWriter(file,true);
			PrintWriter out = new PrintWriter(new BufferedWriter(fileWriter));
			System.out.println("Input name phone : ");
			System.out.println("Enter *end* for stop add :");
			String line = buffer.readLine();
			int i = 1;
			while (!line.equals("end")) {
				if(i==1){out.println();i=0;}
				out.println(line);
				line = buffer.readLine();
			}
			out.flush();
		}
		catch (NumberFormatException e){
			System.err.println("Plese enter name phone!!");
		}
		catch (FileNotFoundException e){
			System.err.println("Cannot read file : "+file);
		}
		catch (IOException e){
			System.err.println("Error reading from file");
		}
		
		finally {
			try {
				if (fileWriter!= null)
					fileWriter.close();
			} catch (IOException e) {
			System.err.println("Error closing files");
			}
		}

	}

}
